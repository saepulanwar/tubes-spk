-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2020 at 03:09 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `career_rise`
--

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `id_karyawan` varchar(5) NOT NULL,
  `NIP` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(150) NOT NULL,
  `jabatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id_karyawan`, `NIP`, `nama`, `alamat`, `jabatan`) VALUES
('KR001', '02780011', 'Dana Sulistiyo Kusumo', '', 'Direktur'),
('KR002', '96700036', 'Warsino', '', 'Manager Bidang Umum '),
('KR003', '12770013', 'Indah Purnomowati', '', 'Manager Marketing '),
('KR004', '13820075', 'Kemas Muslim Lhaksamana ', '', 'Manager Inskubasi Bisnis '),
('KR005', '14750043', 'Soni Fajar Surya Gumilang ', '', 'Manager Solusi Teknologi '),
('KR006', '91650034', 'Lilis Soenaryati ', '', 'Asman Finance and Quality '),
('KR007', '14900001', 'Fika Deningtyas', '', 'Asman Secretariat and Tenant Service '),
('KR008', '15890045', 'Eko Rahayu', '', 'Asman Inkubasi Bisni'),
('KR009', '14890064', 'Resha Akbar', '', 'Staf Marketing '),
('KR010', '15900040', 'Donni Richasdy ', '', 'Staf  Solusi Teknologi '),
('KR011', '15900042', 'Ahmad Syaifudin', '', 'Staf Hilirisasi Riset '),
('KR012', '14850020', 'Amir Hamzah ', '', 'Staf Tenant Service '),
('KR013', '17900088', 'Hilman Ibnu Assiddiq', '', 'Staf Solusi Teknologi '),
('KR014', '19920024', 'M. Galuh Yudhistira ', '', 'Staf Marketing '),
('KR015', '0', 'Sari Widyastuti', '', 'Staf Administrasu Bidang Keuangan '),
('KR016', '00', 'Reza Restinda', '', 'Staf Administrasi Bidang Inkubasi '),
('KR017', '000', 'Belian Tampubolon', '', 'Staf Solusi Teknologi '),
('KR018', '0000', 'Rakhmat Pamungkas', '', 'Staf Solusi Teknologi'),
('KR019', '00000', 'Deni Imam Firmansyah', '', 'Staf Solusi Teknologi ');

-- --------------------------------------------------------

--
-- Table structure for table `kriteria_kinerja`
--

CREATE TABLE `kriteria_kinerja` (
  `id_kriteria` varchar(5) NOT NULL,
  `pilihan` varchar(20) NOT NULL,
  `bobot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kriteria_kinerja`
--

INSERT INTO `kriteria_kinerja` (`id_kriteria`, `pilihan`, `bobot`) VALUES
('KN01', 'Sangat Baik', 5),
('KN02', 'Baik', 4),
('KN03', 'Kurang', 3),
('KN04', 'Sangat Kurang', 2);

-- --------------------------------------------------------

--
-- Table structure for table `kriteria_loyalitas`
--

CREATE TABLE `kriteria_loyalitas` (
  `id_kriteria` varchar(5) NOT NULL,
  `pilihan` varchar(20) NOT NULL,
  `bobot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kriteria_loyalitas`
--

INSERT INTO `kriteria_loyalitas` (`id_kriteria`, `pilihan`, `bobot`) VALUES
('LY01', 'Sangat Baik', 5),
('LY02', 'Baik', 4),
('LY03', 'Kurang', 3),
('LY04', 'Sangat Kurang', 2);

-- --------------------------------------------------------

--
-- Table structure for table `kriteria_masa_kerja`
--

CREATE TABLE `kriteria_masa_kerja` (
  `id_kriteria` varchar(5) NOT NULL,
  `pilihan` varchar(20) NOT NULL,
  `bobot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kriteria_masa_kerja`
--

INSERT INTO `kriteria_masa_kerja` (`id_kriteria`, `pilihan`, `bobot`) VALUES
('MK01', '>5 tahun', 5),
('MK02', '3-5 tahun', 4),
('MK03', '1-3 tahun', 3),
('MK04', '< 1 tahun', 2);

-- --------------------------------------------------------

--
-- Table structure for table `kriteria_pendidikan_pelatihan`
--

CREATE TABLE `kriteria_pendidikan_pelatihan` (
  `id_kriteria` varchar(5) NOT NULL,
  `pilihan` varchar(20) NOT NULL,
  `bobot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kriteria_pendidikan_pelatihan`
--

INSERT INTO `kriteria_pendidikan_pelatihan` (`id_kriteria`, `pilihan`, `bobot`) VALUES
('SK01', 'Sangat Baik', 5),
('SK02', 'Baik', 4),
('SK03', 'Kurang', 3),
('SK04', 'Sangat Kurang', 2);

-- --------------------------------------------------------

--
-- Table structure for table `kriteria_promosi`
--

CREATE TABLE `kriteria_promosi` (
  `id_kriteria` varchar(5) NOT NULL,
  `pilihan` varchar(20) NOT NULL,
  `bobot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kriteria_promosi`
--

INSERT INTO `kriteria_promosi` (`id_kriteria`, `pilihan`, `bobot`) VALUES
('PR01', 'Ada', 1),
('PR02', 'Tidak ada', 0);

-- --------------------------------------------------------

--
-- Table structure for table `penilaian`
--

CREATE TABLE `penilaian` (
  `id_karyawan` varchar(5) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kriteria_1` int(11) NOT NULL,
  `kriteria_2` int(11) NOT NULL,
  `kriteria_3` int(11) NOT NULL,
  `kriteria_4` int(11) NOT NULL,
  `kriteria_5` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penilaian`
--

INSERT INTO `penilaian` (`id_karyawan`, `nama`, `kriteria_1`, `kriteria_2`, `kriteria_3`, `kriteria_4`, `kriteria_5`) VALUES
('KR001', 'Dana Sulistiyo Kusumo', 5, 5, 3, 3, 0),
('KR002', 'Warsino', 3, 3, 2, 2, 0),
('KR003', 'Indah Purnomowati', 2, 2, 5, 4, 0),
('KR004', 'Kemas Muslim Lhaksamana', 3, 3, 3, 2, 1),
('KR005', 'Soni Fajar Surya Gumilang', 2, 2, 1, 1, 0),
('KR006', 'Lilis Soenaryati', 1, 1, 5, 3, 1),
('KR007', 'Fika Deningtyas', 2, 2, 4, 4, 0),
('KR008', 'Eko Rahayu ', 3, 3, 2, 2, 0),
('KR009', 'Resha Akbar ', 5, 5, 2, 3, 0),
('KR010', 'Donni Richasdy ', 3, 3, 3, 2, 1),
('KR011', 'Ahmad Syaifudin ', 4, 4, 4, 3, 0),
('KR012', 'Amir Hamzah ', 4, 4, 4, 2, 0),
('KR013', 'Hilman Ibnu Assiddiq ', 2, 2, 2, 3, 0),
('KR014', 'M. Galuh Yudhistira ', 3, 3, 4, 3, 0),
('KR015', 'Sari Widyastuti ', 3, 3, 3, 2, 0),
('KR016', 'Reza Restinda', 2, 2, 3, 4, 0),
('KR017', 'Belian Tampubolon', 2, 2, 2, 2, 0),
('KR018', 'Rakhmat Pamungkas', 2, 2, 3, 3, 0),
('KR019', 'Deni Imam Firmansyah', 4, 4, 5, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_karyawan` varchar(5) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` enum('user','admin','leader') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_karyawan`, `nama`, `username`, `password`, `level`) VALUES
('KR001', 'Dana Sulistiyo Kusumo', 'dana', '1234', 'leader'),
('KR002', 'Warsino', 'warsino', '1234', 'admin'),
('KR003', 'Indah Purnomowati', 'indah', '1234', 'admin'),
('KR004', 'Kemas Muslim Lhaksamana', 'kemas', '1234', 'user'),
('KR005', 'Soni Fajar Surya Gumilang ', 'soni', '1234', 'admin'),
('KR010', 'Donni Richasdy', 'donni', '1234', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id_karyawan`),
  ADD UNIQUE KEY `NIP` (`NIP`),
  ADD KEY `NIP_2` (`NIP`);

--
-- Indexes for table `kriteria_kinerja`
--
ALTER TABLE `kriteria_kinerja`
  ADD PRIMARY KEY (`id_kriteria`);

--
-- Indexes for table `kriteria_loyalitas`
--
ALTER TABLE `kriteria_loyalitas`
  ADD PRIMARY KEY (`id_kriteria`);

--
-- Indexes for table `kriteria_masa_kerja`
--
ALTER TABLE `kriteria_masa_kerja`
  ADD PRIMARY KEY (`id_kriteria`);

--
-- Indexes for table `kriteria_pendidikan_pelatihan`
--
ALTER TABLE `kriteria_pendidikan_pelatihan`
  ADD PRIMARY KEY (`id_kriteria`);

--
-- Indexes for table `kriteria_promosi`
--
ALTER TABLE `kriteria_promosi`
  ADD PRIMARY KEY (`id_kriteria`);

--
-- Indexes for table `penilaian`
--
ALTER TABLE `penilaian`
  ADD PRIMARY KEY (`id_karyawan`),
  ADD UNIQUE KEY `id` (`id_karyawan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `penilaian`
--
ALTER TABLE `penilaian`
  ADD CONSTRAINT `penilaian_ibfk_1` FOREIGN KEY (`id_karyawan`) REFERENCES `karyawan` (`id_karyawan`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_karyawan`) REFERENCES `karyawan` (`id_karyawan`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
