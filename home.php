<style>
  .edit{
    background-color: #2c7df5;
    text-align: center;
    border-radius: 8px;
    color: #ffff;
  }

</style>
<div class="row">
          <div class="col-md-12">
            <div class="container-fluid" style="background-color: white; border-radius: 10px;">
                <h2 style="text-align: center; padding-top: 15px;"><b>About App</b></h2>
                <p style="padding: 10px; text-align: justify;">SPK yang kami rancang ini merupakan sistem yang dapat memberikan solusi mengenai daftar karyawan yang memiliki kelayakan untuk mendapatkan kenaikan jabatan dengan batasan jumlah tertentu. Setiap karyawan dapat masuk ke dalam daftar tersebut berdasarkan pertimbangan dari beberapa aspek yang menjadi faktor penilaian di dalam perusahaan tempat bekerja. Dari daftar yang muncul, bagian SDM dapat memilih beberapa atau semua karyawan tersebut untuk mendapatkan kenaikan jabatan.  </p>
              
            </div>
          </div>
        </div>