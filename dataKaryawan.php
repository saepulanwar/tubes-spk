<style>
  .edit{
    background-color: #2c7df5;
    text-align: center;
    border-radius: 8px;
    color: #ffff;
  }
  .delete{
    background-color: #fc0000;
    text-align: center;
    border-radius: 8px;
    color: #ffff;
  }

</style>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "career_rise";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT id_karyawan, NIP, nama, jabatan, alamat FROM karyawan";
$sql2 = "SELECT id_karyawan, nama, kriteria_1, kriteria_2, kriteria_3, kriteria_4, kriteria_5 FROM penilaian";


$result = $conn->query($sql);
$result2 = $conn->query($sql2);


?>
<div class="row">
    <div class="col-md-12">
        <div class="container-fluid" style="background-color: white; border-radius: 10px;">
            <p class="table-title">Daftar Karyawan</p>
            <table>
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>NIP</th>
                    <th>Nama</th>
                    <th>Jabatan</th>
                    <th>Alamat</th>
                    <?php 
                    if($_SESSION['akun_level']=='admin'){
                    echo 
                    "<th>*</th>";
                    }?>
                </tr>
                <?php
                $num = 1;
                  if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                ?>
                <tr>
                    <td><?php echo $num++;?></td>
                    <td><?php echo $row['id_karyawan'];?></td>
                    <td><?php echo $row['NIP'];?></td>
                    <td><?php echo $row['nama'];?></td>
                    <td><?php echo $row['jabatan'];?></td>
                    <td><?php echo $row['alamat'];?></td>
                    <?php 
                    if($_SESSION['akun_level']=='admin'){
                    echo 
                    "<td align='center'>
                    <a id='edit_data' data-toggle='modal' data-target='#edit-karyawan' data-id_karyawan='"
                    .$row['id_karyawan']."'"."data-nip='".$row['NIP']."'". "data-nama='".$row['nama']."'"."data-jabatan='".$row['jabatan']."'"."data-alamat='".$row['alamat']."'>
                        <button style='border-radius:8px;' class='btn btn-primary btn-xs'><i class='fa fa-edit'></i>Edit</button>
                    </a>
                    <button style='border-radius:8px;' class='btn btn-danger btn-xs'><i class='fa fa-trash'></i>Hapus</button>
                    </td>";
                }?>
                </tr>
                <?php
                    }
                  } else {
                    echo "0 results";
                  }
                  // $conn->close();
                ?>
            </table>                        
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="container-fluid" style="background-color: white; border-radius: 10px;">
            <p class="table-title">Data Karyawan + Penilaian</p>
            <table>
                <tr>
                    <th>#</th>
                    <th>Alternatif</th>
                    <th>Masa Kerja</th>
                    <th>Pendidikan</th>
                    <th>Kinerja</th>
                    <th>Loyalitas</th>
                    <th>Promosi</th>
                    <?php 
                    if($_SESSION['akun_level']!='user'){
                    echo 
                    "<th>*</th>";
                }?>
                </tr>
                <?php
                $num = 1;
                  if ($result2->num_rows > 0) {
                    // output data of each row
                    while($row2 = $result2->fetch_assoc()) {
                ?>
                <tr>
                    <td><?php echo $num++;?></td>
                    <td><?php echo $row2['nama'];?></td>
                    <td><?php echo $row2['kriteria_1'];?></td>
                    <td><?php echo $row2['kriteria_2'];?></td>
                    <td><?php echo $row2['kriteria_3'];?></td>
                    <td><?php echo $row2['kriteria_4'];?></td>
                    <td><?php echo $row2['kriteria_5'];?></td>
                    
                    <?php 
                    if($_SESSION['akun_level']=='admin'){
                    echo 
                    "<td align='center'>
                    <a id='edit_nilai' data-toggle='modal' data-target='#edit-nilai' data-id_karyawan2='".$row2['id_karyawan']."'"."data-nama2='".$row2['nama']."'". "data-c1='".$row2['kriteria_1']."'". "data-c2='".$row2['kriteria_2']."'"."data-c3='".$row2['kriteria_3']."'"."data-c4='".$row2['kriteria_4']."'".">
                        <button style='border-radius:8px;' class='btn btn-primary btn-xs'><i class='fa fa-edit'></i>Edit</button>
                    </a>
                    <button style='border-radius:8px;' class='btn btn-danger btn-xs'><i class='fa fa-trash'></i>Hapus</button>
                    </td>";
                }?>
                <?php 
                    if($_SESSION['akun_level']=='leader'){
                    echo 
                    "<td align='center'>
                    <a id='edit_promo' data-toggle='modal' data-target='#edit-promo' data-id_karyawan3='".$row2['id_karyawan']."'"."data-nama3='".$row2['nama']."'". "data-c5='".$row2['kriteria_5']."'".">
                        <button style='border-radius:8px;' class='btn btn-primary btn-xs'><i class='fa fa-edit'></i>Promosikan</button>
                    </a>";

                }?>
                </tr>
                <?php
                    }
                  } else {
                    echo "0 results";
                  }
                  // $conn->close();
                ?>
            </table>                        
        </div>
    </div>
</div>
<!-- Modal pop up data karyawan -->
<div class="modal fade" id="edit-karyawan" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Form Perubahan Data Karyawan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="form" enctype="multipart/form-data">
        <div class="modal-body" id="modal-edit">
            <div class="form-group">
                <label for="nip">NIP </label>
                <input  type="hidden" id="id_karyawan" name="id_karyawan">
                <input type="text" name="nip" class="form-control" id="nip" required>
            </div>
            <div class="form-group">
                <label for="bobot">Nama</label>
                <input type="text" name="nama" class="form-control" id="nama" required>
            </div>
            <div class="form-group">
                <label for="bobot">Jabatan</label>
                <input type="text" name="jabatan" class="form-control" id="jabatan" required>
            </div>
            <div class="form-group">
                <label for="bobot">Alamat</label>
                <input type="text" name="alamat" class="form-control" id="alamat">
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
        <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
        </div>
        </form>
    </div>
    </div>
</div>

<!-- Modal pop up penilaian -->
<div class="modal fade" id="edit-nilai" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Form Penilaian Karyawan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="form-penilaian" enctype="multipart/form-data">
        <div class="modal-body" id="modal-edit">
        
            <div class="form-group">
                <label for="nip">Nama : <p name="nama" id="nama"></p> </label>
                <!-- <label for="nip">NIP : <p name="nip" id="nip"></p> </label> -->
                <input  type="hidden" id="id_karyawan2" name="id_karyawan2">
                <!-- <input type="text" name="nama" class="form-control" id="nama" required> -->
            </div>
            <div class="form-group">
                <label for="bobot">Masa Kerja</label>
                <input type="text" name="c1" class="form-control" id="c1" required>
            </div>
            <div class="form-group">
                <label for="bobot">Pendidikan Pelatihan</label>
                <input type="text" name="c2" class="form-control" id="c2" required>
            </div>
            <div class="form-group">
                <label for="bobot">Kinerja</label>
                <input type="text" name="c3" class="form-control" id="c3" required>
            </div>
            <div class="form-group">
                <label for="bobot">Loyalitas</label>
                <input type="text" name="c4" class="form-control" id="c4" required>
            </div>
            <!-- <div class="form-group">
                <label for="bobot">C4</label>
                <select class="dropdown" id = "c4" name="c4" required>
                    <option value = "0">0</option>
                    <option value = "1">1</option>
                </select>
            </div> -->
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
        <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
        </div>
        </form>
    </div>
    </div>
</div>
<!-- pop up promosi -->
<div class="modal fade" id="edit-promo" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Form Promosi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="form-promosi" enctype="multipart/form-data">
        <div class="modal-body" id="modal-edit">
        
            <div class="form-group">
                <label for="nip">Nama : <p name="nama" id="nama"></p> </label>
                <!-- <label for="nip">NIP : <p name="nip" id="nip"></p> </label> -->
                <input  type="hidden" id="id_karyawan3" name="id_karyawan3">
                <!-- <input type="text" name="nama" class="form-control" id="nama" required> -->
            </div>
            <div class="form-group">
                <label for="bobot">Promosikan karyawan ini?</label>
                <select class="dropdown form-control" id = "c5" name="c5" required>
                    <option value = "0">Tidak</option>
                    <option value = "1">Promosikan</option>
                </select>
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
        <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
        </div>
        </form>
    </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/jquery.easydropdown.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).on("click", "#edit_data", function() {
    var id = $(this).data('id_karyawan');
    var nip = $(this).data('nip');
    var nama = $(this).data('nama');
    var jabatan = $(this).data('jabatan');
    var alamat = $(this).data('alamat');
    $("#modal-edit #id_karyawan").val(id);
    $("#modal-edit #nip").val(nip);
    $("#modal-edit #nama").val(nama);
    $("#modal-edit #jabatan").val(jabatan);
    $("#modal-edit #alamat").val(alamat);
})

$(document).ready(function(e) {
    $("#form").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'edit_karyawan.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=dataKaryawan";
    }));
});

// penilaian
$(document).on("click", "#edit_nilai", function() {
    var id = $(this).data('id_karyawan2');
    var nama = $(this).data('nama2');
    var c1 = $(this).data('c1');
    var c2 = $(this).data('c1');
    var c3 = $(this).data('c3');
    var c4 = $(this).data('c4');
    $("#modal-edit #id_karyawan2").val(id);
    $("#modal-edit #nama").text(nama);
    $("#modal-edit #c1").val(c1);
    $("#modal-edit #c2").val(c2);
    $("#modal-edit #c3").val(c3);
    $("#modal-edit #c4").val(c4);
})

$(document).ready(function(e) {
    $("#form-penilaian").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'edit_karyawan.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=dataKaryawan";
    }));
    
});

$(document).on("click", "#edit_promo", function() {
    var id = $(this).data('id_karyawan3');
    var nama = $(this).data('nama3');
    var c5 = $(this).data('c5');
    $("#modal-edit #id_karyawan3").val(id);
    $("#modal-edit #nama").text(nama);
    $("#modal-edit #c5").val(c5);
})

$(document).ready(function(e) {
    $("#form-promosi").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'edit_karyawan.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=dataKaryawan";
    }));
    
});
</script>