<style>
  .edit{
    background-color: #2c7df5;
    text-align: center;
    border-radius: 8px;
    color: #ffff;
  }

</style>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "career_rise";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT id_kriteria, pilihan, bobot FROM kriteria_masa_kerja";
$sql2 = "SELECT id_kriteria, pilihan, bobot FROM kriteria_pendidikan_pelatihan";
$sql3 = "SELECT id_kriteria, pilihan, bobot FROM kriteria_kinerja";
$sql4 = "SELECT id_kriteria, pilihan, bobot FROM kriteria_promosi";
$sql5 = "SELECT id_kriteria, pilihan, bobot FROM kriteria_loyalitas";

$result = $conn->query($sql);
$result2 = $conn->query($sql2);
$result3 = $conn->query($sql3);
$result4 = $conn->query($sql4);
$result5 = $conn->query($sql5);

?>
<div class="row">
          <div class="col-md-6">
            <div class="container-fluid" style="background-color: white; border-radius: 10px;">
              <p>Kriteria Masa Kerja</p>
              <table>
                <tr>
                  <th>#</th>
                  <th>Pilihan</th>
                  <th>Nilai</th>
                  <th>*</th>
                </tr>
                <?php
                $num = 1;
                  if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                ?>
                <tr>
                <td><?php echo $num++;?></td>
                <td><?php echo $row['pilihan'];?></td>
                <td><?php echo $row['bobot'];?></td>
                <td align="center">
                  <a id="edit_data" data-toggle="modal" data-target="#edit" data-id_kriteria="<?php echo $row['id_kriteria']?>" data-pilihan="<?php echo $row['pilihan']?>" data-bobot="<?php echo $row['bobot']?>">
                    <button style="border-radius:8px;" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i>Edit</button>
                  </a>
                  <!-- <button style="border-radius:8px;" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i>Hapus</button> -->
                </td>
                </tr>
                <?php
                    }
                  } else {
                    echo "0 results";
                  }
                  // $conn->close();
                ?>
              </table>
            </div>
          </div>
          <div class="col-md-6">
            <div class="container-fluid" style="background-color: white; border-radius: 10px;">
              <p>Pendidikan Pelatihan</p>
              <table>
                <tr>
                  <th>#</th>
                  <th>Pilihan</th>
                  <th>Nilai</th>
                  <th>*</th>
                </tr>
                <?php
                $num = 1;
                  if ($result2->num_rows > 0) {
                    // output data of each row
                    while($row2 = $result2->fetch_assoc()) {
                ?>
                <tr>
                <td><?php echo $num++;?></td>
                <td><?php echo $row2['pilihan'];?></td>
                <td><?php echo $row2['bobot'];?></td>
                <td align="center">
                  <a id="edit_data" data-toggle="modal" data-target="#edit" data-id_kriteria="<?php echo $row2['id_kriteria']?>" data-pilihan="<?php echo $row2['pilihan']?>" data-bobot="<?php echo $row2['bobot']?>">
                    <button style="border-radius:8px;" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i>Edit</button>
                  </a>
                  <!-- <button style="border-radius:8px;" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i>Hapus</button> -->
                </td>
                </tr>
                <?php
                    }
                  } else {
                    echo "0 results";
                  }
                  // $conn->close();
                ?>
              </table>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="container-fluid" style="background-color: white; border-radius: 10px;">
              <p>Kriteria Kinerja</p>
              <table>
                <tr>
                  <th>#</th>
                  <th>Pilihan</th>
                  <th>Nilai</th>
                  <th>*</th>
                </tr>
                <?php
                $num = 1;
                  if ($result3->num_rows > 0) {
                    // output data of each row
                    while($row3 = $result3->fetch_assoc()) {
                ?>
                <tr>
                <td><?php echo $num++;?></td>
                <td><?php echo $row3['pilihan'];?></td>
                <td><?php echo $row3['bobot'];?></td>
                <td align="center">
                  <a id="edit_data" data-toggle="modal" data-target="#edit" data-id_kriteria="<?php echo $row3['id_kriteria']?>" data-pilihan="<?php echo $row3['pilihan']?>" data-bobot="<?php echo $row3['bobot']?>">
                    <button style="border-radius:8px;" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i>Edit</button>
                  </a>
                  <!-- <button style="border-radius:8px;" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i>Hapus</button> -->
                </td>
                </tr>
                <?php
                    }
                  } else {
                    echo "0 results";
                  }
                  // $conn->close();
                ?>
              </table>
            </div>
          </div>
          <div class="col-md-6">
            <div class="container-fluid" style="background-color: white; border-radius: 10px;">
              <p>Kriteria Loyalitas</p>
              <table>
                <tr>
                  <th>#</th>
                  <th>Pilihan</th>
                  <th>Nilai</th>
                  <th>*</th>
                </tr>
                <?php
                $num = 1;
                  if ($result5->num_rows > 0) {
                    // output data of each row
                    while($row5 = $result5->fetch_assoc()) {
                ?>
                <tr>
                <td><?php echo $num++;?></td>
                <td><?php echo $row5['pilihan'];?></td>
                <td><?php echo $row5['bobot'];?></td>
                <td align="center">
                  <a id="edit_data" data-toggle="modal" data-target="#edit" data-id_kriteria="<?php echo $row5['id_kriteria']?>" data-pilihan="<?php echo $row5['pilihan']?>" data-bobot="<?php echo $row5['bobot']?>">
                    <button style="border-radius:8px;" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i>Edit</button>
                  </a>
                  <!-- <button style="border-radius:8px;" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i>Hapus</button> -->
                </td>
                </tr>
                <?php
                    }
                  } else {
                    echo "0 results";
                  }
                  // $conn->close();
                ?>
              </table>
            </div>
          </div>
        
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="container-fluid" style="background-color: white; border-radius: 10px;">
              <p>Kriteria Promosi</p>
              <table>
                <tr>
                  <th>#</th>
                  <th>Pilihan</th>
                  <th>Nilai</th>
                  <th>*</th>
                </tr>
                <?php
                $num = 1;
                  if ($result4->num_rows > 0) {
                    // output data of each row
                    while($row4 = $result4->fetch_assoc()) {
                ?>
                <tr>
                <td><?php echo $num++;?></td>
                <td><?php echo $row4['pilihan'];?></td>
                <td><?php echo $row4['bobot'];?></td>
                <td align="center">
                  <a id="edit_data" data-toggle="modal" data-target="#edit" data-id_kriteria="<?php echo $row4['id_kriteria']?>" data-pilihan="<?php echo $row4['pilihan']?>" data-bobot="<?php echo $row4['bobot']?>">
                    <button style="border-radius:8px;" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i>Edit</button>
                  </a>
                  <!-- <button style="border-radius:8px;" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i>Hapus</button> -->
                </td>
                </tr>
                <?php
                    }
                  } else {
                    echo "0 results";
                  }
                  // $conn->close();
                ?>
              </table>
            </div>
          </div>
        </div>
        <!-- Modal pop up -->
        <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Form Perubahan Nilai Kriteria</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form id="form" enctype="multipart/form-data">
              <div class="modal-body" id="modal-edit">
                  <div class="form-group">
                    <label for="pilihan">Pilihan </label>
                    <input type="hidden" id="id_kriteria" name="id_kriteria">
                    <input type="text" name="pilihan" class="form-control" id="pilihan" required>
                  </div>
                  <div class="form-group">
                    <label for="bobot">Bobot</label>
                    <input type="number" name="bobot" class="form-control" id="bobot" required>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
                <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
              </div>
              </form>
            </div>
          </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script type="text/javascript">
          $(document).on("click", "#edit_data", function() {
            var id = $(this).data('id_kriteria');
            var pil = $(this).data('pilihan');
            var bob = $(this).data('bobot');
            $("#modal-edit #id_kriteria").val(id);
            $("#modal-edit #pilihan").val(pil);
            $("#modal-edit #bobot").val(bob);
          })

          $(document).ready(function(e) {
            $("#form").on("submit", (function(e) {
              
              e.preventDefault();
              $.ajax({
                url:'edit_data.php',
                type: 'POST',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function(msg) {
                  $('.table').html(msg);
                }
              });
              window.location="?page=dataKriteria";
            }));
          });
        </script>