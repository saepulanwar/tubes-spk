<style>
  .edit{
    background-color: #2c7df5;
    text-align: center;
    border-radius: 8px;
    color: #ffff;
  }
  .delete{
    background-color: #fc0000;
    text-align: center;
    border-radius: 8px;
    color: #ffff;
  }

</style>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "career_rise";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT id_karyawan, nama, kriteria_1, kriteria_2, kriteria_3, kriteria_4, kriteria_5 FROM penilaian";
$sql2 = "SELECT id_karyawan, nama, kriteria_1, kriteria_2, kriteria_3, kriteria_4, kriteria_5 FROM penilaian";
$sql3 = "SELECT penilaian.id_karyawan, penilaian.nama, penilaian.kriteria_1, penilaian.kriteria_2, penilaian.kriteria_3, penilaian.kriteria_4, penilaian.kriteria_5, karyawan.NIP, karyawan.nama, karyawan.id_karyawan,karyawan.jabatan FROM penilaian JOIN karyawan ON karyawan.id_karyawan=penilaian.id_karyawan";
$sql4= "SELECT max(kriteria_1) AS maxC1, max(kriteria_2) AS maxC2, max(kriteria_3) AS maxC3, max(kriteria_4) AS maxC4, max(kriteria_5) AS maxC5 FROM penilaian";

$result = $conn->query($sql);
$result2 = $conn->query($sql2);
$result3 = $conn->query($sql3);
$result4= $conn->query($sql4);

foreach($result as $key=>$value){
    $k1[]=$value['kriteria_1'];
    $k2[]=$value['kriteria_2'];
    $k3[]=$value['kriteria_3'];
    $k4[]=$value['kriteria_4'];
    $k5[]=$value['kriteria_5'];
}

foreach($result4 as $key=>$value){
    $max1=$value['maxC1'];
    $max2=$value['maxC2'];
    $max3=$value['maxC3'];
    $max4=$value['maxC4'];
    $max5=$value['maxC5'];
}

foreach($result as $key=>$value) {
      $n1[]=$value['kriteria_1']/$max1;
      $n2[]=$value['kriteria_2']/$max2;
      $n3[]=$value['kriteria_3']/$max3;
      $n4[]=$value['kriteria_4']/$max4;
      $n5[]=$value['kriteria_5']/$max5;
  }
// print_r($n1);
// echo $n1;
?>
<div class="row">
    <div class="col-md-12">
        <div class="container-fluid" style="background-color: white; border-radius: 10px;">
            <p class="table-title">Normalisasi</p>
            <table>
                <tr>
                    <th>#</th>
                    <th>Alternatif</th>
                    <th>Masa Kerja</th>
                    <th>Pendidikan</th>
                    <th>Kinerja</th>
                    <th>Loyalitas</th>
                    <th>Promosi</th>
                </tr>
                <?php
                $num = 1;
                  if ($result2->num_rows > 0) {
                    // output data of each row
                    while($row = $result2->fetch_assoc()) {
                ?>
                <tr>
                    <td><?php echo $num++;?></td>
                    <td><?php echo $row['nama'];?></td>
                    <td><?php echo round($row['kriteria_1']/$max1,2);?></td>
                    <td><?php echo round($row['kriteria_2']/$max2,2);?></td>
                    <td><?php echo round($row['kriteria_3']/$max3,2);?></td>
                    <td><?php echo round($row['kriteria_4']/$max4,2);?></td>
                    <td><?php echo round($row['kriteria_5']/$max5,2);?></td>
                </tr>
                <?php
                    }
                  } else {
                    echo "0 results";
                  }
                  // $conn->close();
                ?>
            </table>                       
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="container-fluid" style="background-color: white; border-radius: 10px;">
            <p class="table-title"><b>Nilai Ranking</b></p>
            <table id="nilai_akhir">
                <tr>
                    <th>#</th>
                    <th>Alternatif</th>
                    <th>Masa Kerja</th>
                    <th>Pendidikan</th>
                    <th>Kinerja</th>
                    <th>Loyalitas</th>
                    <th>Promosi</th>
                    <th>Total</th>
                    <?php 
                    if($_SESSION['akun_level']=='admin'){
                    echo 
                    "<th>Action</th>";
                    }?>
                </tr>
                <?php
                $num = 1;
                  if ($result3->num_rows > 0) {
                    // output data of each row
                    while($row3 = $result3->fetch_assoc()) {
                        // while($row4 = $result4->fetch_assoc()){
                ?>
                <tr>
                    <td><?php echo $num++;?></td>
                    <td><?php echo $row3['nama'];?></td>
                    <td><?php echo round(($row3['kriteria_1']/$max1)*0.09,2);?></td>
                    <td><?php echo round(($row3['kriteria_2']/$max2)*0.09,2);?></td>
                    <td><?php echo round(($row3['kriteria_3']/$max3)*0.12,2);?></td>
                    <td><?php echo round(($row3['kriteria_4']/$max4)*0.05,2);?></td>
                    <td><?php echo round(($row3['kriteria_5']/$max5)*0.65,2);?></td>
                    <td style="background-color:<?php if (round(((($row3['kriteria_1']/$max1)*0.09)+(($row3['kriteria_2']/$max2)*0.09)+(($row3['kriteria_3']/$max3)*0.12)+(($row3['kriteria_4']/$max4)*0.05)+(($row3['kriteria_5']/$max5)*0.65))*100,2)>50){echo "rgb(77,251,204);"; }?>"><?php echo round(((($row3['kriteria_1']/$max1)*0.09)+(($row3['kriteria_2']/$max2)*0.09)+(($row3['kriteria_3']/$max3)*0.12)+(($row3['kriteria_4']/$max4)*0.05)+(($row3['kriteria_5']/$max5)*0.65))*100,2)?>%</td>
                    <?php 
                    if($_SESSION['akun_level']=='admin'){
                    echo 
                    "<td>
                    <a id='edit_data' data-toggle='modal' data-target='#edit-jabatan' data-id_karyawan='".$row3['id_karyawan']."'". "data-jabatan='".$row3['jabatan']."'". "data-nama='".$row3['nama']."'". "data-nip='".$row3['NIP']."'". "data-c1='".$row3['kriteria_1']."'". "data-c2='".$row3['kriteria_2']."'". "data-c3='".$row3['kriteria_3']."'"."data-c4='".$row3['kriteria_4']."'".">
                        <button style='border-radius:8px;' class='btn btn-primary btn-xs'><i class='fa fa-edit'></i>Naikan Jabatan</button>
                    </a>
                        <!-- <button style='border-radius:8px;' class='btn btn-primary btn-xs'><i class='fa fa-edit'></i>Ubah Jabatan</button> -->
                        <!-- <button style='border-radius:8px;' class='btn btn-danger btn-xs'><i class='fa fa-edit'></i>Tunda</button> -->
                
                    </td>";
                    }?>
                </tr>
                <?php
                    }
                // }
                  } else {
                    echo "0 results";
                  }
                  // $conn->close();
                ?>
            </table>                  
        </div>
    </div>
</div>

<!-- Modal pop up data Jabatan -->
<div class="modal fade" id="edit-jabatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Form Perubahan Jabatan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="form" enctype="multipart/form-data">
        <div class="modal-body" id="modal-edit">
            <div class="form-group">
                <label for="nama">Nama : <span name="nama" id="nama"></span> </label>
                <input  type="hidden" id="id_karyawan" name="id_karyawan">
                <input  type="hidden" id="c1" name="c1">
                <input  type="hidden" id="c2" name="c2">
                <input  type="hidden" id="c3" name="c3">
                <input  type="hidden" id="c4" name="c4">
                <input  type="hidden" id="c5" name="c5">

                <!-- <input type="text" name="nama" class="form-control" id="nama" readonly> -->
            </div>
            <div class="form-group">
                <label for="nip">NIP : <span name="nip" id="nip"></span> </label>
                <!-- <input type="text" name="nip" class="form-control" id="nip" readonly> -->
            </div>
            <div class="form-group">
                <label for="bobot">Jabatan Baru</label>
                <input type="text" name="jabatan" class="form-control" id="jabatan" required>
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
        <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
        </div>
        </form>
    </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/jquery.easydropdown.js" type="text/javascript"></script>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="jquery.tablesort.js"></script>

<script type="text/javascript">


$(document).on("click", "#edit_data", function() {
    var id = $(this).data('id_karyawan');
    var nip = $(this).data('nip');
    var nama = $(this).data('nama');
    var jabatan = $(this).data('jabatan');
    var c1 = 0;
    var c2 = 0;
    var c3 = 0;
    var c4 = 0;
    var c5 = 0;
    $("#modal-edit #id_karyawan").val(id);
    $("#modal-edit #nip").text(nip);
    $("#modal-edit #nama").text(nama);
    $("#modal-edit #jabatan").val(jabatan);
    $("#modal-edit #c1").val(c1);
    $("#modal-edit #c2").val(c2);
    $("#modal-edit #c3").val(c3);
    $("#modal-edit #c4").val(c4);
    $("#modal-edit #c4").val(c5);
})

$(document).ready(function(e) {
    $("#form").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'edit_nilaiAkhir.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=perhitunganAkhir";
    }));
});

// penilaian
// $(document).on("click", "#edit_nilai", function() {
//     var id = $(this).data('id_karyawan2');
//     var nama = $(this).data('nama2');
//     var c1 = $(this).data('c1');
//     var c2 = $(this).data('c1');
//     var c3 = $(this).data('c3');
//     var c4 = $(this).data('c4');
//     $("#modal-edit #id_karyawan2").val(id);
//     $("#modal-edit #nama").text(nama);
//     $("#modal-edit #c1").val(c1);
//     $("#modal-edit #c2").val(c2);
//     $("#modal-edit #c3").val(c3);
//     $("#modal-edit #c4").val(c4);
// })

// $(document).ready(function(e) {
//     $("#form-penilaian").on("submit", (function(e) {
//     e.preventDefault();
//     $.ajax({
//         url:'edit_karyawan.php',
//         type: 'POST',
//         data: new FormData(this),
//         contentType: false,
//         cache: false,
//         processData: false,
//         success: function(msg) {
//         $('.table').html(msg);
//         }
//     });
//     window.location="?page=dataKaryawan";
//     }));
    
// });
</script>