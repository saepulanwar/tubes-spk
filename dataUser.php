<style>
  .edit{
    background-color: #2c7df5;
    text-align: center;
    border-radius: 8px;
    color: #ffff;
  }
  .delete{
    background-color: #fc0000;
    text-align: center;
    border-radius: 8px;
    color: #ffff;
  }

</style>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "career_rise";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql2 = "SELECT id_karyawan, nama, username, password, level FROM user";
$sql = "SELECT id_karyawan, nama FROM karyawan";

$result2 = $conn->query($sql2);
$result = $conn->query($sql);



?>

<div class="row">
    <div class="col-md-12">
        <div class="container-fluid" style="background-color: white; border-radius: 10px;">
            <p class="table-title">Data User 
            <a id="tambah_user" data-toggle="modal" data-target="#tambah-user">
            <button style='border-radius:8px;' class='btn btn-success btn-xs'><i class='fa fa-plus'></i>Tambah User Baru</button></a></p>
            <table>
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Username</th>
                    <th>Level</th>
                    <th>Password</th>
                    <th>Action</th>
                </tr>
                <?php
                $num = 1;
                  if ($result2->num_rows > 0) {
                    // output data of each row
                    while($row2 = $result2->fetch_assoc()) {
                ?>
                <tr>
                    <td><?php echo $num++;?></td>
                    <td><?php echo $row2['id_karyawan'];?></td>
                    <td><?php echo $row2['nama'];?></td>
                    <td><?php echo $row2['username'];?></td>
                    <td><?php echo $row2['level'];?></td>
                    <td><?php echo $row2['password'];?></td>
                    
                    <?php 
                    if($_SESSION['akun_level']=='admin'){
                    echo 
                    "<td align='center'>
                    <a id='edit_nilai' data-toggle='modal' data-target='#edit-nilai' data-id_karyawan2='".$row2['id_karyawan']."'"."data-nama='".$row2['nama']."'"."data-username='".$row2['username']."'"."data-password='".$row2['password']."'"."data-level='".$row2['level']."'".">
                        <button style='border-radius:8px;' class='btn btn-primary btn-xs'><i class='fa fa-edit'></i>Edit</button>
                    </a>
                    <button style='border-radius:8px;' class='btn btn-danger btn-xs'><i class='fa fa-trash'></i>Hapus</button>
                    </td>";
                }?>
                </tr>
                <?php
                    }
                  } else {
                    echo "0 results";
                  }
                  // $conn->close();
                ?>
            </table>                        
        </div>
    </div>
</div>

<!-- Modal pop up edit user -->
<div class="modal fade" id="edit-nilai" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Form Perubahan Data User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="form-penilaian" enctype="multipart/form-data">
        <div class="modal-body" id="modal-edit">
        
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" name="nama" class="form-control" id="nama" required>
                <input  type="hidden" id="id_karyawan2" name="id_karyawan2">
            </div>
            <div class="form-group">
                <label for="username">username</label>
                <input type="text" name="username" class="form-control" id="username" required>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="text" name="password" class="form-control" id="password" required>
            </div>
            <div class="form-group">
                <label for="level">Level</label>
                <select class="dropdown" id = "level" name="level" required>
                    <option value = "admin">Admin</option>
                    <option value = "user">User</option>
                    <option value = "leader">Leader</option>
                </select>
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
        <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
        </div>
        </form>
    </div>
    </div>
</div>
<!-- pop up tambah user -->
<div class="modal fade" id="tambah-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Form Tambah User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form id="form-tambah" enctype="multipart/form-data">
        <div class="modal-body" id="modal-edit">
            <div class="form-group">
                <label for="id_karyawan">ID</label>
                <!-- <input type="text" id="id_karyawan" class="form-control" name="id_karyawan" required> -->
                <select class="dropdown form-control" name="id_karyawan" id="id_karyawan" class="dropdown" required>
                <?php
                    foreach($result as $row=>$value){
                        echo "<option value='".$value['id_karyawan']."'".">".$value['id_karyawan']."</option>";
                    }
                ?>
                </select>
            </div>
            <div class="form-group">
                <label for="nama">Nama</label>
                <!-- <input type="text" id="nama" name="nama" class="form-control" required> -->
                <select class="dropdown form-control" name="nama" id="nama" class="dropdown" required>
                <?php
                    foreach($result as $row=>$value){
                        echo "<option value='".$value['nama']."'".">".$value['nama']."</option>";
                    }
                ?>
                </select>
            </div>
            <div class="form-group">
                <label for="username">username</label>
                <input type="text" name="username" class="form-control" id="username" required>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="text" name="password" class="form-control" id="password" required>
            </div>
            <div class="form-group">
                <label for="level">Level</label>
                <select class="dropdown form-control" id = "level" name="level">
                    <option value = "admin">Admin</option>
                    <option value = "user">User</option>
                    <option value = "leader">Leader</option>
                </select>
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="reset" class="btn btn-danger">Reset</button> -->
        <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
        </div>
        </form>
    </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/jquery.easydropdown.js" type="text/javascript"></script>
<script type="text/javascript">
// penilaian
$(document).on("click", "#edit_nilai", function() {
    var id = $(this).data('id_karyawan2');
    var nama = $(this).data('nama');
    var username = $(this).data('username');
    var level = $(this).data('level');
    var password = $(this).data('password');
    $("#modal-edit #id_karyawan2").val(id);
    $("#modal-edit #nama").val(nama);
    $("#modal-edit #username").val(username);
    $("#modal-edit #level").val(level);
    $("#modal-edit #password").val(password);
})

$(document).ready(function(e) {
    $("#form-penilaian").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'edit_user.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=dataUser";
    }));
    
});

$(document).ready(function(e) {
    $("#form-tambah").on("submit", (function(e) {
    e.preventDefault();
    $.ajax({
        url:'tambah_user.php',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(msg) {
        $('.table').html(msg);
        }
    });
    window.location="?page=dataUser";
    }));
    
});

</script>