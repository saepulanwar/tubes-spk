<?php
ob_start();
session_start();
if(isset($_SESSION['username'])) header("location: index.php");
include "config.php";

if(isset($_POST['submit_login'])){
    $username=$_POST['username'];
    $pass=$_POST['password'];
    $sql_login= mysqli_query($conn, "SELECT * FROM user WHERE username = '$username' AND password='$pass'");
    if(mysqli_num_rows($sql_login)>0){
        $row_akun = mysqli_fetch_array($sql_login);
        $_SESSION['akun_id']=$row_akun['id_karyawan'];
		$_SESSION['akun_username']=$row_akun['username'];
		$_SESSION['akun_level']=$row_akun['level'];
        header("location:index.php");
    }
    else{
        header("location:login.php?login-gagal");
    }

}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="dist/css/util.css">
    <link rel="stylesheet" type="text/css" href="dist/css/main.css">
    <script src="https://kit.fontawesome.com/a8a64df7c3.js" crossorigin="anonymous"></script>
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
            <!-- <div class="welcome">
                <h1 class="welcome">WELCOME</h1>
                <h2 class="login">PLEASE LOGIN!</h2>
            </div> -->
			<div class="wrap-login100 p-l-50 p-r-50 p-t-77 p-b-30">
				<form class="login100-form validate-form" action="" method="post">
					<span class="login100-form-title p-b-55">
						Login
					</span>

					<div class="wrap-input100 validate-input m-b-16" data-validate = "Valid username is required: admin">
                    
                        <input class="input100" type="text" name="username" placeholder="username">
                        
						<span class="focus-input100"></span>
						<span class="symbol-input100">
                            <i class="fas fa-user-circle"></i>
						</span>
                    </div>

					<div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-lock"></span>
						</span>
                    </div>
                    <?php
                        if(isset($_GET['login-gagal'])){
                    ?>
                    <div>
                        <p style="color:red;">Periksa kembali username atau password yang anda masukan!</p>
                    </div>
                    <?php
                        }
                    ?>
					<div class="contact100-form-checkbox m-l-4">
						<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
						<label class="label-checkbox100" for="ckb1">
							Remember me
						</label>
					</div>
					
					<div class="container-login100-form-btn p-t-25">
						<button class="login100-form-btn" type="submit" name="submit_login">
							Login
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="dist/js/main.js"></script>

</body>
</html>
<?php
mysqli_close($conn);
ob_end_flush();
?>
